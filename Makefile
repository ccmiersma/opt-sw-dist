all: build 

build: build-config.rb src/bashlib.sh.erb src/profile.sh.erb
	mkdir build 
	erb -r ./build-config.rb src/bashlib.sh.erb > build/bashlib.sh
	erb -r ./build-config.rb src/profile.sh.erb > build/opt-sw-dist.sh


install:	build/bashlib.sh build/opt-sw-dist.sh README.md
	install -d -m 755 \
		./build/buildroot/opt/share/doc/opt-sw-dist/example/src/ \
		./build/buildroot/etc/profile.d/ \
		./build/buildroot/opt/share/opt-sw-dist/  \
		./build/buildroot/opt/bin \
		./build/buildroot/opt/sbin \
		./build/buildroot/opt/libexec \
		./build/buildroot/opt/share \
		./build/buildroot/etc/opt\
		./build/buildroot/var/opt\
		./build/buildroot/var/opt/lib\
		./build/buildroot/opt/lib\
		./build/buildroot/opt/include\
		./build/buildroot/opt/share/info\
		./build/buildroot/opt/share/man
	install -m 644 build/bashlib.sh ./build/buildroot/opt/share/opt-sw-dist/ 
	install -m 644 build/opt-sw-dist.sh ./build/buildroot/etc/profile.d/
	install -m 644 README.md LICENSE ./build/buildroot/opt/share/doc/opt-sw-dist/
	install -m 644 Makefile.erb opt-sw-dist.spec ./build/buildroot/opt/share/doc/opt-sw-dist/example/
	install -m 755 configure ./build/buildroot/opt/share/doc/opt-sw-dist/example/
	install -m 644 src/* ./build/buildroot/opt/share/doc/opt-sw-dist/example/src/

clean:
	rm -rf build

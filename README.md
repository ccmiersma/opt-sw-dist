Opt Software Distribution
=========================

This package will install the base environment for a custom software distribution in /opt.
This can serve as a starting point for a complete integrated set of scripts, configurations,
and custom applications local to your organization.

This sets up a software tree similar to /usr/ or /usr/local under /opt/, or
the path of your choice. This allows packaged scripts to be easily distinguished
from the operating system and third-party vendors. A number of locations such as /opt/bin, /opt/sbin
are mentioned in the [FHS](http://www.pathname.com/fhs/pub/fhs-2.3.html#OPTADDONAPPLICATIONSOFTWAREPACKAGES), but reserved for local use. This package sets up those directories, with some adjustments.

In addition, if it is built with the `--local_prefix` option, then it will do the same thing for
a subdirectory of /opt. If this is built as an RPM, it the subdirectory of /opt will depend on the local_prefix macro, which will be automatically set from the release if nothing is inherited. The default release is `1.local%{?dist}` which means that the RPM will install locations under /opt/local. You can create your own release by replacing `local` with your own organization name. If it is built with an empty local_prefix macro, it will use /opt directly.

This is achieved by using ERB templates to generate a Makefile and then building shell scripts from ERB templates. This allows the shell scripts to adjust to the way the RPM is built or other configuration. Rather the using an overwhelming number of variables making readability difficult, generating the shell scripts from templates allows the most common paths to be explicitly set, while only those elements than need variables get them.



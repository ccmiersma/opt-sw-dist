%define author Christopher Miersma

Name:		opt-sw-dist
Version:        0.2.1
Release:        1.local%{?dist}
Summary:	Custom Software Distribution in /opt
Group:		local
License:	MIT
URL:		https://gitlab.com/ccmiersma/opt-sw-dist/
Source0:	%{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  ruby
BuildRequires:  rubygems
BuildRequires:  make
Provides: local-sw-dist
Obsoletes: local-sw-dist

%description
This package will install the base environment for a custom software distribution in /opt.
This can serve as a starting point for a complete integrated set of scripts, configurations,
and custom applications local to your organization.

# This allows the local_prefix to be overridden by external macros. If it is not overridden,
# The spec file will attempt to auto generate it from the release.
%{!?local_prefix:%define local_prefix %(echo %{release} | cut -f2 -d. | egrep -v "(^el[[:digit:]]|^ol[[:digit:]]|^fc[[:digit:]])" )}

#This spec file can generate multiple packages using the local_prefix; the key differences are below.

%if "%{local_prefix}" != "" && "%{local_prefix}" != "git"
%define _prefix /opt/%{local_prefix}
%else
%define local_prefix %(echo "")
%define _prefix /opt
%endif

%define _sysconfdir /etc/%{_prefix}
%define _datadir %{_prefix}/share
%define _docdir %{_datadir}/doc
%define _mandir %{_datadir}/man
%define _bindir %{_prefix}/bin
%define _sbindir %{_prefix}/sbin
%define _lib lib
%define _libdir %{_prefix}/%_lib
%define _libexecdir %{_prefix}/libexec
%define _includedir %{_prefix}/include
%define _localstatedir /var/%{_prefix}
%define _sharedstatedir %{_localstatedir}/lib

%prep
%setup

# The actual work of configuring building and installing has been offloaded to 
# configure and make scripts. In this case, autotools are not used. The configure
# script uses ERB templates to generate the Makefile. The scripts are also generated from templates.
# 
%build

./configure --local_prefix=%{local_prefix} --destdir=%{buildroot}
make

%install

make install


%clean
%__rm -rf %buildroot

%files
%defattr(-,root,root, -)
%if "%{local_prefix}" != ""
%dir %_prefix
%dir %_localstatedir
%dir %_sysconfdir
%endif
%dir %_datadir
%dir %_docdir
%dir %_mandir
%dir %_bindir
%dir %_sbindir
%dir %_libdir
%dir %_libexecdir
%dir %_includedir
%dir %_sharedstatedir
%dir %_datadir/%name
%config /etc/profile.d/%{name}.sh
%_datadir/%name/bashlib.sh
%doc %_docdir/%name/


# The post and postun update the man page database
%post


%postun


%changelog
* Fri Dec 08 2017 Christopher Miersma <ccmiersma@gmail.com> 0.2.1-1.local
- Updated documentation. (ccmiersma@gmail.com)
- Fixed CI config for install (ccmiersma@gmail.com)
- Adjusted make file for simpler install (ccmiersma@gmail.com)
- Adjusted make file for simpler install (ccmiersma@gmail.com)
- Added Gitlab CI. (ccmiersma@gmail.com)

* Fri Dec 08 2017 Christopher Miersma <ccmiersma@gmail.com> 0.2.0-1.local
- Fix duplicate path entires. (ccmiersma@gmail.com)
- Replace old local-sw-dist (ccmiersma@gmail.com)
- Updated makefile to add examples. (ccmiersma@gmail.com)
- Updated makefile to add examples. (ccmiersma@gmail.com)
- Updated makefile to add examples. (ccmiersma@gmail.com)



